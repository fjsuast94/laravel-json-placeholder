<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        pre {
            border-radius: .5rem!important;
            margin-top: 1.5rem!important;
            margin-bottom: 1.5rem!important;
            padding: 1.5rem!important;
        }
    </style>
    <!-- prism CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.21.0/themes/prism-tomorrow.min.css" rel="stylesheet">

    <title>Laravel Json-PlaceHolder</title>
  </head>
  <body cz-shortcut-listen="true">
    
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
      <div class="container">
        <a class="navbar-brand" href="/">Laravel {JSON}</a>
      </div>
    </nav>
    
    <main class="container">
        <div class="m-5 rounded">
            <h1>Guide</h1>
            <p class="lead">Below you'll find examples using <a href="#">Fetch API</a> but you can JSONPlaceholder with any other language.</p>
            <p class="lead">You can copy paste the code in your browser console to quickly test JSONPlaceholder.</p>
        </div>

        <div class="m-5 rounded">
            <h1>Getting a resource</h1>
            <pre class=" language-js"><code class=" language-js"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'https://laravel-json-placeholder.herokuapp.com/api/posts/1'</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">response</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> response<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">json</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>json<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
              </code></pre>
            <p>Output</p>
            <pre class=" language-js"><code class=" language-js"><span class="token punctuation">{</span>
    id<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    title<span class="token operator">:</span> <span class="token string">'...'</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token string">'...'</span><span class="token punctuation">,</span>
    userId<span class="token operator">:</span> <span class="token number">1</span>
<span class="token punctuation">}</span></code></pre>
            <h1>Listing all resources</h1>
            <pre class=" language-js"><code class=" language-js"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'https://laravel-json-placeholder.herokuapp.com/api/posts'</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">response</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> response<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">json</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>json<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    </code></pre>
    <p>Output</p>
    <pre class=" language-js"><code class=" language-js"><span class="token punctuation">[</span>
    <span class="token punctuation">{</span> id<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> title<span class="token operator">:</span> <span class="token string">'...'</span> <span class="token comment">/* ... */</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span> id<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> title<span class="token operator">:</span> <span class="token string">'...'</span> <span class="token comment">/* ... */</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span> id<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> title<span class="token operator">:</span> <span class="token string">'...'</span> <span class="token comment">/* ... */</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token comment">/* ... */</span>
    <span class="token punctuation">{</span> id<span class="token operator">:</span> <span class="token number">100</span><span class="token punctuation">,</span> title<span class="token operator">:</span> <span class="token string">'...'</span> <span class="token comment">/* ... */</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">]</span><span class="token punctuation">;</span>
      </code></pre>
      <h1>Creating a resource</h1>
      <pre class=" language-js"><code class=" language-js"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'https://laravel-json-placeholder.herokuapp.com/api/posts'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'POST'</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token constant">JSON</span><span class="token punctuation">.</span><span class="token function">stringify</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
        title<span class="token operator">:</span> <span class="token string">'foo'</span><span class="token punctuation">,</span>
        body<span class="token operator">:</span> <span class="token string">'bar'</span><span class="token punctuation">,</span>
        userId<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
    headers<span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token string">'Content-type'</span><span class="token operator">:</span> <span class="token string">'application/json; charset=UTF-8'</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">response</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> response<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">json</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>json<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
      </code></pre>
      <p>Output</p>
      <pre class=" language-js"><code class=" language-js"><span class="token punctuation">{</span>
    id<span class="token operator">:</span> <span class="token number">101</span><span class="token punctuation">,</span>
    title<span class="token operator">:</span> <span class="token string">'foo'</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token string">'bar'</span><span class="token punctuation">,</span>
    userId<span class="token operator">:</span> <span class="token number">1</span>
<span class="token punctuation">}</span>
      </code></pre>
      <h1>Updating a resource</h1>
      <pre class=" language-js"><code class=" language-js"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'https://laravel-json-placeholder.herokuapp.com/api/posts/1'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'PUT'</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token constant">JSON</span><span class="token punctuation">.</span><span class="token function">stringify</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
        id<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        title<span class="token operator">:</span> <span class="token string">'foo'</span><span class="token punctuation">,</span>
        body<span class="token operator">:</span> <span class="token string">'bar'</span><span class="token punctuation">,</span>
        userId<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
    headers<span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token string">'Content-type'</span><span class="token operator">:</span> <span class="token string">'application/json; charset=UTF-8'</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">response</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> response<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">json</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>json<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
    <p>Output</p>
    <pre class=" language-js"><code class=" language-js"><span class="token punctuation">{</span>
    id<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    title<span class="token operator">:</span> <span class="token string">'foo'</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token string">'bar'</span><span class="token punctuation">,</span>
    userId<span class="token operator">:</span> <span class="token number">1</span>
<span class="token punctuation">}</span></code></pre>
        <h1>Deleting a resource</h1>
        <pre class=" language-js"><code class=" language-js"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'https://laravel-json-placeholder.herokuapp.com/api/posts/1'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'DELETE'</span><span class="token punctuation">,</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
        </div>

        <div class="m-5 rounded">
            <h1>Routes</h1>
            <p class="lead">All HTTP methods are supported. You can use http or https for your requests.:</p>
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/posts">/api/posts</a></td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/posts/1">/api/posts/1</a></td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/posts/1/comments">/api/posts/1/comments</a></td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/comments?postId=1">/api/comments?postId=1</a></td>
                    </tr>
                    <tr>
                        <td>POST</td>
                        <td></td>
                        <td>/api/posts</td>
                    </tr>
                    <tr>
                        <td>PUT</td>
                        <td></td>
                        <td>/api/posts/1</td>
                    </tr>
                    <tr>
                        <td>PATCH</td>
                        <td></td>
                        <td>/api/posts/1</td>
                    </tr>
                    <tr>
                        <td>DELETE</td>
                        <td></td>
                        <td>/api/posts/1</td>
                    </tr>
                </tbody>
            </table>
            <p class="mt-3"> 
                <strong>Note:</strong> resources have relations. For example: posts have many comments, a
                lbums have many photos, ... see guide for the full list.
            </p>
        </div>

    </main>
    <footer class="container">
        <p>© 2017–2021 Laravel {JSON} Placeholder · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.21.0/prism.min.js"></script>

    <script>

    </script>
  </body>
</html>
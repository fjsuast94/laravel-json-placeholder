<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        pre {
            border-radius: .5rem!important;
            margin-top: 1.5rem!important;
            margin-bottom: 1.5rem!important;
            padding: 1.5rem!important;
        }
    </style>
    <!-- prism CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.21.0/themes/prism-tomorrow.min.css" rel="stylesheet">

    <title>Laravel Json-PlaceHolder</title>
  </head>
  <body cz-shortcut-listen="true">
    
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
      <div class="container">
        <a class="navbar-brand" href="/">Laravel {JSON}</a>
      </div>
    </nav>
    
    <main class="container">
      <div class="bg-light m-5 p-5 rounded">
          <h1>Laravel {JSON} Placeholder</h1>
          <p class="lead">Powered by Laravel + Mysql.</p>
        </div>
        <div class="m-5 rounded">
            <h1>Try it</h1>
            <p class="lead">Run this code here, in a console or from any site:</p>
            <pre class=" language-javascript"><code id="example" class=" language-javascript"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'https://laravel-json-placeholder.herokuapp.com/api/todos/1'</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">response</span> <span class="token operator">=&gt;</span> response<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">json</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>json<span class="token punctuation">)</span><span class="token punctuation">)</span></code></pre>
            <button type="button" id="buttonScript" class="btn btn-success">Run Script</button>
            <pre class="language-javascript"><code id="result" class=" language-javascript"> <span class="token punctuation">{</span><span class="token punctuation">}</span></code></pre>
        </div>

        <div class="m-5 rounded">
            <h1>When to use</h1>
            <p class="lead">Laravel JSONPlaceholder is a free online REST API that you can use whenever you need some fake data. It can be in a README on GitHub, 
                for a demo on CodeSandbox, in code examples on Stack Overflow, ...or simply to test things locally.
            </p>
        </div>
        
        <div class="m-5 rounded">
            <h1>Resources</h1>
            <p class="lead">JSONPlaceholder comes with a set of 6 common resources:</p>
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="/api/posts">/api/posts</a></td>
                        <td>100 posts</td>
                    </tr>
                    <tr>
                        <td><a href="/api/comments">/api/comments</a></td>
                        <td>500 comments</td>
                    </tr>
                    <tr>
                        <td><a href="/api/albums">/api/albums</a></td>
                        <td>100 albums</td>
                    </tr>
                    <tr>
                        <td><a href="/api/photos">/api/photos</a></td>
                        <td>5000 photos</td>
                    </tr>
                    <tr>
                        <td><a href="/api/todos">/api/todos</a></td>
                        <td>200 todos</td>
                    </tr>
                    <tr>
                        <td><a href="/api/users">/api/users</a></td>
                        <td>10 users</td>
                    </tr>
                </tbody>
            </table>
            <p class="mt-3"> 
                <strong>Note:</strong> resources have relations. For example: posts have many comments, a
                lbums have many photos, ... see guide for the full list.
            </p>
        </div>

        <div class="m-5 rounded">
            <h1>Routes</h1>
            <p class="lead">All HTTP methods are supported. You can use http or https for your requests.:</p>
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/posts">/api/posts</a></td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/posts/1">/api/posts/1</a></td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/posts/1/comments">/api/posts/1/comments</a></td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td></td>
                        <td><a href="/api/comments?postId=1">/api/comments?postId=1</a></td>
                    </tr>
                    <tr>
                        <td>POST</td>
                        <td></td>
                        <td>/api/posts</td>
                    </tr>
                    <tr>
                        <td>PUT</td>
                        <td></td>
                        <td>/api/posts/1</td>
                    </tr>
                    <tr>
                        <td>PATCH</td>
                        <td></td>
                        <td>/api/posts/1</td>
                    </tr>
                    <tr>
                        <td>DELETE</td>
                        <td></td>
                        <td>/api/posts/1</td>
                    </tr>
                </tbody>
            </table>
            <p class="mt-3"> 
                Note:
                <strong>Note:</strong> see <a href="/guide">guide</a> for usage examples.
            </p>
        </div>

    </main>
    <footer class="container">
        <p>© 2017–2021 Laravel {JSON} Placeholder · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.21.0/prism.min.js"></script>

    <script>
        const runScript = () => {
            const result = document.getElementById("result");
            fetch('/api/todos/1')
            .then(response => response.json())
            .then(json => {
            result.innerHTML = `<span class="token punctuation">{</span>    
<span class="token string">"userId"</span><span class="token operator">:</span> <span class="token number">${json.userId}</span><span class="token punctuation">,</span>
<span class="token string">"id"</span><span class="token operator">:</span> <span class="token number">${json.id}</span><span class="token punctuation">,</span>
<span class="token string">"title"</span><span class="token operator">:</span> <span class="token string">"${json.title}"</span><span class="token punctuation">,</span>
<span class="token string">"completed"</span><span class="token operator">:</span> <span class="token boolean">${json.completed}</span>
<span class="token punctuation">}</span>`
        })
    }
        const buttonScript =  document.getElementById("buttonScript");
        buttonScript.addEventListener("click", (e) =>{
            runScript()
        })

    </script>
  </body>
</html>
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'postId'  => $this->post->id,
            'id'    => $this->id,
            'name'  => $this->name,
            'email'  => $this->email,
            'body'  => $this->body,
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Company;
use App\Models\Geolocation;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory(10)->create();

        foreach ($users as $user) {
            $addresses = Address::factory(1)->create([
                'user_id' => $user->id
            ]);

            Company::factory(1)->create([
                'user_id' => $user->id
            ]);

            Geolocation::factory(1)->create([
                'address_id' => $addresses[0]->id
            ]);
        }
    }
}

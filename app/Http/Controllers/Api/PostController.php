<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CommentCollection;
use App\Http\Resources\PostCollection;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;

class PostController extends ApiController
{

    const POST_KEY = 'post_key';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PostCollection::collection(
        Cache::remember(static::POST_KEY, 60, function(){
                    return Post::with("user")->get();
                })
            );
    }

    public function comments(Request $request)
    {
        $params = $request->all();
        $query = Comment::with(["post"]);
        $search = Arr::get($params, 'postId', '');

        $query->when($search, function ($q) use ($search){
            $q->where("post_id", $search);
        });

        return CommentCollection::collection($query->get());
    }
    public function postWithComment($id)
    {
        try {
            $post = Post::with("comments.post")->findOrFail($id);
            return CommentCollection::collection($post->comments);
        } catch (ModelNotFoundException $th) {
            return response()->json(new \stdClass());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => 'required|numeric',
            'title' => 'required',
            'body' => 'required',
        ]);

        return new PostCollection(Post::create($validated));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return new PostCollection(Post::with("user")->findOrFail($id));
        } catch (ModelNotFoundException $th) {
            return response()->json(new \stdClass());
        }
    }

    public function showComment($id)
    {
        try {
            return new CommentCollection(Comment::with("post")->findOrFail($id));
        } catch (ModelNotFoundException $th) {
            return response()->json(new \stdClass());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'user_id' => 'required|numeric',
            'title' => 'required',
            'body' => 'required',
        ]);
        
        try {
            Post::findOrFail($id)->update($validated);
            $post = Post::findOrFail($id);
            return new PostCollection($post);
        } catch (ModelNotFoundException $th) {
            return response()->json(new \stdClass());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Post::findOrFail($id)->delete();
        } catch (ModelNotFoundException $th) {
            return response()->json(new \stdClass());
        }
    }
}

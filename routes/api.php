<?php

use App\Http\Controllers\Api\AlbumController;
use App\Http\Controllers\Api\PhotoController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\TodoController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('/users', UserController::class);
Route::resource('/posts', PostController::class);
Route::get('/posts/{id}/comments/', [PostController::class, 'postWithComment']);
Route::resource('/todos', TodoController::class);
Route::resource('/photos', PhotoController::class);
Route::resource('/albums', AlbumController::class);
Route::get('/comments', [PostController::class, 'comments']);
Route::get('/comments/{id}', [PostController::class, 'showComment']);



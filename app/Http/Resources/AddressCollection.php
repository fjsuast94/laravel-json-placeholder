<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'street'  => $this->street,
            'suite'  => $this->suite,
            'city'  => $this->city,
            'zipcode'  => $this->zipcode,
            'geo' => new GeolocationCollection($this->geo)
        ];
    }
}

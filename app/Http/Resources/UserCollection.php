<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'username'  => $this->username,
            'email'  => $this->email,
            'address'  => new AddressCollection( $this->address),
            'phone'  => $this->phone,
            'website'  => $this->website,
            'company'  => new CompanyCollection($this->company),
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\Album;
use App\Models\Comment;
use App\Models\Photo;
use App\Models\Post;
use App\Models\Todo;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        Todo::factory(200)->create();
        Album::factory(100)->create();
        Photo::factory(5000)->create();
        Post::factory(100)->create();
        Comment::factory(500)->create();
    }
}
